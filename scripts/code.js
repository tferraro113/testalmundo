$(function(){
	$(".collapse:not(#filtros-container)").on('shown.bs.collapse', function () {
   		$(this).prev().find(".collapser").removeClass("fa-caret-down").addClass("fa-caret-up");
	}); 

	$(".collapse:not(#filtros-container)").on('hidden.bs.collapse', function () {
    	$(this).prev().find(".collapser").removeClass("fa-caret-up").addClass("fa-caret-down");
	});

	$(".collapse#filtros-container").on('shown.bs.collapse', function () {
   		$(".filtrar-caret").removeClass("fa-caret-down").addClass("fa-caret-up");
	}); 

	$(".collapse#filtros-container").on('hidden.bs.collapse', function () {
    	$(".filtrar-caret").removeClass("fa-caret-up").addClass("fa-caret-down");
	});

	$('.carousel').carousel({
    	interval: false
	}); 

})

	

