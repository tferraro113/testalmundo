var app = angular.module("almundoApp", ['rzModule']);

app.controller("hotelesController",[ '$scope', 'hotelesFactory' , function($scope , hotelesFactory ) {

	$scope.q = "";
	$scope.starsArray = {};
	$scope.sortTypes = [{
		value : 'none',
		label : 'Seleccione',
	},{
		value : 'price',
		label : 'Precio Más Bajo',
	},{
		value: '-price',
		label: 'Precio más alto'
	},{
		value: 'stars',
		label: 'Menos Estrellas'
	},{
		value: '-stars',
		label: 'Más Estrellas'
	}];
	$scope.slider = {
	  minValue: 100,
	  maxValue: 2000,
	  options: {
	    floor: 780,
	    ceil: 2500,
	    minRange: 300,
	    pushRange: true,
	    translate: function(value) {
	      return '$' + value;
	    }
	  }
	};

	$scope.submit = function (){
		$scope.q = $scope.query;
	}

	hotelesFactory.get().then(function(data){
		$scope.hoteles = data.hotels;
	})

	$scope.resetStarsArray = function(){
		$scope.starsArray = {};
	}

	$scope.getStars = function (stars){
		return new Array(parseInt(stars));
	}

}]);


app.filter('rangeFilter',[ function(){
	return function (hotels,range){
		var filtered = [];
		angular.forEach(hotels, function(hotelItem){
			var price = hotelItem.price;
			if(price >= range.minValue && price <= range.maxValue){
				filtered.push(hotelItem);
			}

		})
		return filtered;
	};
}]);

app.filter('starsFilter',[ function(){
	return function (hotels,stars){
		var filtered = [];
		var falseFlag = false;
		
		angular.forEach(stars, function (value, key){
			if(value){
				console.log(value);
				falseFlag = true;
			}
		})

		if(!falseFlag){
			return hotels;
		}

		angular.forEach(hotels, function(hotelItem){
			angular.forEach(stars, function (starsValue, starsKey ){
				if(hotelItem.stars == starsKey && starsValue != false){
					filtered.push(hotelItem);
				}
			})

		})
		return filtered;
	};
}]);

app.factory("hotelesFactory", [ '$q' , '$http' , function( $q , $http) {
	var deferred = $q.defer();

	$http({
		method: 'GET',
		url: 'scripts/response.json'
	}).then(function(success){
		deferred.resolve(success.data);
	}, function (error){
		throw new Error("response.json, file not found");
	})

	return {
		get: function (){
			return deferred.promise
		}
	}

}]);