#Exámen Almundo

##Cómo instalar dependencias ?

```
bower install
npm install
```

En caso de que no se tenga instalado el gulp-cli global correr :
```
sudo npm install --global gulp-cli
```

! También debe estar instalado **sass** para que corra desde la consola. En caso de no tenerlo se puede cambiar la hoja de estilos por la que está en "css/styles.css"

##Cómo correr proyecto ?

```
gulp watch
```

El proyecto se verá en "http://localhost:4000/" en su navegador.

Gracias